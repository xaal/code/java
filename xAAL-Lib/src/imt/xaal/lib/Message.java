package imt.xaal.lib;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;

import com.upokecenter.cbor.CBORObject;

public class Message {
    private int version;
    private long[] timestamp;
    private List<UUID> targets=null;
    private UUID source=null;
    private String devType;
    private MessageType msgType = null;
    private String action;
    private CBORObject body;
    private HashMap<String,CBORObject> bodyAsMap = null;

    Message() {
        version = 7;
        targets = new ArrayList<UUID>();
    }

    public int getVersion() {
        return version;
    }
    public void setVersion(int version) {
        this.version = version;
    }

    public long[] getTimestamp() {
        return timestamp;
    }
    public void setTimestamp(long[] timestamp) {
        this.timestamp = timestamp;
    }

    public List<UUID> getTargets() {
        return targets;
    }
    public void setTargets(List<UUID> targets) {
        this.targets = targets;
    }

    public UUID getSource() {
        return source;
    }
    public void setSource(UUID source) {
        this.source = source;
    }

    public String getDevType() {
        return devType;
    }
    public void setDevType(String devType) {
        this.devType = devType;
    }

    public MessageType getMsgType() {
        return msgType;
    }
    public void setMsgType(MessageType msgType) {
        this.msgType = msgType;
    }

    public String getAction() {
        return action;
    }
    public void setAction(String action) {
        this.action = action;
    }

    public CBORObject getBody() {
        return body;
    }

    public HashMap<String,CBORObject> getBodyAsMap() {
        if (bodyAsMap!=null) return  bodyAsMap;
        HashMap<String,CBORObject> result = new HashMap<>();
        HashMap<CBORObject,CBORObject> hash = body.ToObject(HashMap.class);
        for (CBORObject key : hash.keySet()) {
            String tmp = key.toString().replace("\"", "");
            result.put(tmp, hash.get(key));
        }
        bodyAsMap = result;
        return bodyAsMap;
    }

    public void setBody(CBORObject body) {
        bodyAsMap = null;
        this.body = body;
    }

    public void setBodyAsMap(HashMap<String,Object> body) {
        bodyAsMap = null;
        this.body = CBORObject.FromObject(body);
    }


    public void dump() {
        StringBuilder buf = new StringBuilder();
        buf.append("====== MSG " + this + " ====\n");
        buf.append("version: \t" + version+"\n");
        buf.append("timestamp:\t[" + timestamp[0] + "," + timestamp[1]+"]\n");
        buf.append("target:  \t[");
        for (UUID addr: getTargets()) {
            buf.append(addr+",");
        }
        buf.append("]\n");
        buf.append("dev_type:\t" + devType +"\n");
        buf.append("msg_type:\t" + msgType +"\n");
        buf.append("source: \t" + source +"\n");
        buf.append("action: \t" + action +"\n");
        //buf.append("\n");
        if (body!=null) {
            buf.append("body:   ");
            int i=0;
            for (var k : body.getKeys()) {
                if (i==0) {buf.append("\t" + k + ":" + body.get(k) + "\n");}
                else {buf.append("\t\t" + k + ":" + body.get(k) + "\n");}
                i++;
            }
            //buf.append("\n");
        }
        System.out.println(buf);
    }

    /*
    public String toString() {
        return "[" + getClass().getSimpleName() +" " + source + " " + dev_type + " " + msg_type + " " + action +"]";
    }
    */
    public boolean isRequest() {
        if (msgType==MessageType.REQUEST) return true;
        return false;
    }

    public boolean isReply() {
        if (msgType==MessageType.REPLY) return true;
        return false;
    }

    public boolean isNotify() {
        if (msgType==MessageType.NOTIFY) return true;
        return false;
    }
    
    public boolean isAlive() {
        if (isNotify() && action.equals("alive")) return true;
        return false;
    }

    public boolean isRequestIsAlive() {
        // TODO: filter on dev_type
        if (isRequest() && action.equals("is_alive")) return true;
        return false;
    }

    public boolean isAttributesChange() {
        if (isNotify() && action.equals("attributes_change")) return true;
        return false;
    }

    public boolean isGetAttributeReply() {
        if (isReply() && action.equals("get_attributes")) return true;
        return false;
    }

    public boolean isGetDescriptionReply() {
        if (isReply() && action.equals("get_description")) return true;
        return false;
    }


}

