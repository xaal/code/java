package imt.xaal.lib;

import java.util.HashMap;

public enum MessageType {
    NOTIFY(0), REQUEST(1), REPLY(2);
  
    private final int value;
    private static HashMap<Integer,MessageType> map = new HashMap<Integer,MessageType>();

    private MessageType(int value) {
        this.value = value;
    }

    static {
      for (MessageType msgType : MessageType.values()) {
        map.put(msgType.value, msgType);
      }
    }

    public static MessageType valueOf(int msgType) {
        return (MessageType) map.get(msgType);
    }

    public int getValue() {
        return value;
    }

  }
  