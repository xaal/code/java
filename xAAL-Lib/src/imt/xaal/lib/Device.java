package imt.xaal.lib;

import java.net.URI;
import java.util.ArrayList;
import java.util.List;
import java.util.UUID;
import java.util.logging.Logger;

/**
 *
 * @author jkx
 */
public abstract class Device {

    private final static Logger LOGGER = Logger.getLogger(Device.class.getName());

    // public xAAL data 
    protected String vendorID = null;
    protected String productID = null;
    protected String version = null;
    protected URI url = null;
    protected URI schema = null;
    protected String info = null;
    protected String hwID = null;
    protected UUID groupID = null;

    protected UUID address = null;
    protected String devType = "basic.basic";
    protected Engine engine = null;
    private List<String> unsupportedMethods = new ArrayList<String>();
    private List<String> unsupportedAttributes = new ArrayList<String>();
    private List<String> unsupportedNotifications = new ArrayList<String>();
    private List<Attribute> attributes = new ArrayList<Attribute>();

    // internal stuff
    private int alivePeriod = Config.getInstance().getAliveTimer();
    private long aliveTimeOut = 0;

    public Device(UUID address) {
        setAddress(address);
    }

    /**
     * Every Device should handle its own messages. Just parse the
     * msg.getAction() to find out what to do.
     *
     * @return Message or null
     */
    public abstract Message handleRequest(Message msg);

    public Engine getEngine() {
        return engine;
    }

    public void setEngine(Engine value) {
        engine = value;
    }

    public String getDevType() {
        return devType;
    }

    public void setDevType(String value) {
        devType = value;
    }

    public UUID getAddress() {
        return address;
    }

    public void setAddress(UUID value) {
        address = value;
    }

    public UUID getGroupID() {
        return groupID;
    }

    public void setGroupID(UUID value) {
        groupID = value;
    }

    public String getHwID() {
        return hwID;
    }

    public void setHwID(String value) {
        hwID = value;
    }


    public String getInfo() {
        return info;
    }

    public void setInfo(String value) {
        info = value;
    }


    public URI getSchema() {
        return schema;
    }

    public void setSchema(URI value) {
        schema = value;
    }

    public URI getUrl() {
        return url;
    }

    public void setUrl(URI value) {
        url = value;
    }

    public String getProductID() {
        return productID;
    }

    public void setProductID(String value) {
        productID = value;
    }

    public String getVendorID() {
        return vendorID;
    }

    public void setVendorID(String vendorID) {
        this.vendorID = vendorID;
    }

    public String getVersion() {
        return version;
    }

    public void setVersion(String value) {
        version = value;
    }

    /**
     * set the period between two alive message for this device
     *
     * @param alivePeriod period value
     */
    public void setAlivePeriod(int alivePeriod) {
        this.alivePeriod = alivePeriod;
    }

    /**
     * return the period between too alive message for this device
     *
     * @return period value
     */
    public int getAlivePeriod() {
        return alivePeriod;
    }

    /**
     * return the period timeout announce
     *
     * @return period value
     */
    public int getTimeOutPeriod() {
        return alivePeriod *2;
    }

    /**
     * ask the device to refresh its internal timeout value.
     */
    public void updateAliveTimeOut() {
        aliveTimeOut = System.currentTimeMillis() / 1000 + getAlivePeriod();
    }

    /**
     * get the value of the internal timeout value, it's a timestamp in second
     *
     * @return timestamp in future
     */
    public long getAliveTimeOut() {
        return aliveTimeOut;
    }

    //==========================================================================
    // Unsupported[Methods|Attributes|Notifications] 
    //==========================================================================
    public void setUnsupportedMethods(List<String> values) {
        unsupportedMethods = values;
    }

    public List<String> getUnsupportedMethods() {
        return unsupportedMethods;
    }

    public void setUnsupportedAttributes(List<String> values) {
        unsupportedAttributes = values;
    }

    public List<String> getUnsupportedAttributes() {
        return unsupportedAttributes;
    }

    public void setUnsupportedNotifications(List<String> values) {
        unsupportedNotifications = values;
    }

    public List<String> getUnsupportedNotifications() {
        return unsupportedNotifications;
    }

    //==========================================================================
    // Attributes 
    //==========================================================================
    public void registerAttributes(List<Attribute> attributes) {
        this.attributes.addAll(attributes);
        for (Attribute attr : attributes) {
            attr.setDevice(this);
        }
    }

    public void unregisterAttributes(List<Attribute> attributes) {
        this.attributes.removeAll(attributes);
        for (Attribute attr : attributes) {
            attr.setDevice(null);
        }
    }

    public List<Attribute> getAttributes() {
        return this.attributes;
    }

    //==========================================================================
    // tools 
    //==========================================================================
    public void dump() {
        StringBuilder buf = new StringBuilder();
        buf.append("====== Device " + this + " ====\n");
        buf.append("devType: \t" + getDevType() + "\n");
        buf.append("address: \t" + getAddress() + "\n");
        buf.append("vendorID: \t " + getVendorID() + "\n");
        buf.append("productID: \t" + getProductID() + "\n");
        buf.append("version: \t" + getVersion() + "\n");
        LOGGER.info(buf.toString());

    }

    public String toString() {
        return "[" + getClass().getSimpleName() + " " + getDevType() + " " + getAddress() + "]";
    }

}
