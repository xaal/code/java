package imt.xaal.lib;

/**
 *
 * @author jkx
 */
public class Attribute {

    private String name = "";
    private Object value = null;
    private Device device = null;

    public Attribute(String name, Object defaultValue) {
        setName(name);
        value = defaultValue;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void setValue(Object value) {
        if (value != this.value) {
            this.value = value;
            if (device.getEngine()!=null) {
                device.getEngine().registerAttributeChange(this);
            }
        }
    }

    public Object getValue() {
        return value;
    }

    public void setDevice(Device device) {
        this.device = device;
    }

    public Device getDevice() {
        return device;
    }

    
    public String toString() { 
        return "[" + getClass().getSimpleName() + ": " + this.getName() +" = " + this.getValue()+"]";
    }
}
