package imt.xaal.lib;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.net.NetworkInterface;
import java.net.StandardProtocolFamily;
import java.net.StandardSocketOptions;
import java.nio.ByteBuffer;
import java.nio.channels.DatagramChannel;
import java.nio.channels.MembershipKey;
import java.io.*;
import java.util.Arrays;
import java.util.Queue;
import java.util.logging.Logger;

import imt.xaal.utils.BinUtils;
import imt.xaal.utils.NetworkUtils;

/**
 *
 * @author jkx
 */
public final class NetworkConnector extends Thread {

    private final static Logger LOGGER = Logger.getLogger(NetworkConnector.class.getName());

    private DatagramChannel dgChannel;

    private InetAddress groupAddr;
    private int port;
    private int ttl;
    private boolean pause = false;
    private Queue<byte[]> fifoOUT = null;

    public NetworkConnector(String addr, int port, int ttl) throws IOException {
        setAddr(InetAddress.getByName(addr));
        setPort(port);
        setHops(ttl);
    }

    /**
     * Return Multicast address.
     *
     * @return address
     */
    public InetAddress getAddr() {
        return this.groupAddr;
    }

    /**
     * Return Multicast port
     *
     * @return port number
     */
    public int getPort() {
        return this.port;
    }

    /**
     * get Multicast Time to live
     *
     * @return hops count
     */
    public int getHops() {
        return this.ttl;
    }

    /**
     * Define the xAAL Multicast address
     *
     * @param addr inet address
     */
    public void setAddr(InetAddress addr) {
        this.groupAddr = addr;
    }

    /**
     * Set the xAAL Multicast port
     *
     * @param port int value for the port binding
     */
    public void setPort(int port) {
        this.port = port;
    }

    /**
     * Set the xAAL Multicast time to live
     *
     * @param hops nb to ttl 
     */
    public void setHops(int hops) {
        this.ttl = hops;
   }

    public void connect() throws IOException {
        String ifaceName = NetworkUtils.getDefaultNetworkInterface();
        NetworkInterface iface = NetworkInterface.getByName(ifaceName);

        InetAddress addr = getAddr();
        int port = getPort();
        int hops = getHops();

        this.dgChannel = DatagramChannel.open(StandardProtocolFamily.INET);

        this.dgChannel.setOption(StandardSocketOptions.IP_MULTICAST_IF, iface);
        this.dgChannel.setOption(StandardSocketOptions.IP_MULTICAST_TTL, hops);
        this.dgChannel.setOption(StandardSocketOptions.SO_REUSEPORT, true);

        this.dgChannel.bind(new InetSocketAddress(port));
        this.dgChannel.join(addr, iface);

        LOGGER.info("Connnecting " + addr.toString() + ":" + port);
    }

    public void disconnect() {
        try {
            this.dgChannel.close();
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        LOGGER.info("Leave multicast group");
    }

    public void send(byte[] data) {
        InetSocketAddress target = new  InetSocketAddress(getAddr(),getPort());
        try {
            this.dgChannel.send(ByteBuffer.wrap(data), target);
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
    }

    public byte[] receive() {
        ByteBuffer buf = ByteBuffer.allocate(65507);
        try {
            this.dgChannel.receive(buf);
        } catch (IOException e) {
            LOGGER.severe(e.getMessage());
        }
        byte[] data = new byte[ buf.position() ];  buf.rewind();  buf.get(data);
        return data;
    }

    public void setOUTFifo(Queue<byte[]> value) {
        fifoOUT = value;
    }

    public void pause() {
        pause = true;
    }

    @Override
    public void run() {
        while (pause == false) {
            byte[] data = this.receive();
            if (data != null) {
                if (fifoOUT != null) {
                    fifoOUT.add(data);
                }

            }
        }
    }
}
