package imt.xaal.lib;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.nio.file.Paths;
import java.util.Properties;
import java.util.logging.Logger;

public class Config {
    /* As this API clone the Python one, I decided to reuse the same config file */

    private final static Logger LOGGER = Logger.getLogger(Device.class.getName());
    private static Properties cfg = null;

    private static Config INSTANCE = new Config();
    
    private Config()  {
        String fileName = getFileName();
        LOGGER.info("Loading config" + fileName);

        cfg = new Properties();
        try {
            FileInputStream in = new FileInputStream(fileName);
            cfg.load(in);
        } 
        catch (FileNotFoundException ex) {
            LOGGER.warning("Unable to open xALL config file");
        }
        catch (IOException ex) {
            LOGGER.warning("Unable to parse xAAL confif file");
        }
    }

    public static Config getInstance() {
        return INSTANCE;
    }

    public String getFileName() {
        var path = Paths.get(System.getProperty("user.home"),".xaal", "xaal.ini");
        return path.toString();
    }

    private String getValue(String key,String def) {
        if (cfg==null) return null;
        String value = (String)cfg.get(key);
        if (value==null) return def;
        return value;
    }

    public String getAddress() {
        return getValue("address", "224.0.29.200");
    }

    public Integer getPort() {
        return Integer.parseInt(getValue("port", "1236")) ;
    }

    public Integer getHops() {
        return Integer.parseInt(getValue("hops", "10"));
    }

    public Integer getAliveTimer() {
        return Integer.parseInt(getValue("alive_timer","300"));
    }

    public String getKey() {
        return getValue("key", "");
    }
}
