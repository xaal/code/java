
package imt.xaal.lib;

import java.io.IOException;
import java.security.spec.AlgorithmParameterSpec;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;

import javax.crypto.Cipher;
import javax.crypto.spec.IvParameterSpec;
import javax.crypto.spec.SecretKeySpec;

import com.upokecenter.cbor.CBORObject;

import imt.xaal.utils.BinUtils;
import imt.xaal.utils.UUIDUtils;
import imt.xaal.utils.CBORUtils;



public class MessageFactory {
    private final static Logger LOGGER = Logger.getLogger(Device.class.getName());

    static Random random = new Random();
    private byte[] key;

    MessageFactory(byte[] key) {
        this.key = key;
    }

    public Message decodeMessage(byte[] data) {
        Message msg = new Message();
        CBORObject cbor = CBORUtils.bytesToCbor(data);

        // ------- Headers ---------------------
        // version
        int version = (int)cbor.get(0).AsInt32();
        msg.setVersion(version);
        // timestamp
        long[] ts = new long[2];
        ts[0] = (long)cbor.get(1).AsInt64Value();
        ts[1] = (long)cbor.get(2).AsInt64Value();
        msg.setTimestamp(ts);
        // nonce from timestamp
        byte[] nonce = buildNonce(ts);
        // ad and build target
        byte[] ad = cbor.get(3).GetByteString();
        CBORObject target = CBORUtils.bytesToCbor(ad);
        ArrayList<UUID> targets = new ArrayList<UUID>();
        for (var k: target.getValues()) {
            targets.add(UUIDUtils.getUUIDFromBytes(k.GetByteString()));
        }
        msg.setTargets(targets);

        // ------- Payload --------------------
        byte[] cyph = cbor.get(4).GetByteString();
        byte[] clear;
        try {
            clear = decrypt(cyph, key, nonce, ad);
        } catch (Exception e) {
            LOGGER.warning("Unable to decrypt msg: " + e.toString());
            return null;
        }

        CBORObject payload = CBORUtils.bytesToCbor(clear);
        // source
        UUID source = UUIDUtils.getUUIDFromBytes(payload.get(0).GetByteString());
        msg.setSource(source);
        // dev_type
        String devType = payload.get(1).AsString();
        msg.setDevType(devType);
        // msg_type
        int msgType = payload.get(2).AsInt32();
        msg.setMsgType(MessageType.valueOf(msgType));
        // action
        String action = payload.get(3).AsString();
        msg.setAction(action);
        // body
        if (payload.size()==5) {
            msg.setBody(payload.get(4));
        }
        return msg;
    }


    public byte[] encodeMessage(Message message) throws IOException {
        ArrayList<Object> result = new ArrayList<Object>();
        // ------- Headers ---------------------
        result.add(message.getVersion());
        long[] ts = message.getTimestamp();
        result.add(ts[0]);
        result.add(ts[1]);
        // targets 
        ArrayList<byte[]> targets = new ArrayList<byte[]>();
        for (UUID k:message.getTargets()){
            targets.add(UUIDUtils.getBytesFromUUID(k));
        }
        
        byte[] ad = CBORUtils.cborToBytes(CBORObject.FromObject(targets));
        result.add(ad);

        // ------- Payload --------------------
        ArrayList<Object> buf = new ArrayList<Object>();
        buf.add(UUIDUtils.getBytesFromUUID(message.getSource()));
        buf.add(message.getDevType());
        buf.add(message.getMsgType().getValue());
        buf.add(message.getAction());
        if (message.getBody()!=null) { buf.add(message.getBody()); }
        // cyphering
        byte[] clear = CBORUtils.cborToBytes(CBORObject.FromObject(buf));        
        byte[] nonce = buildNonce(ts);
        try {
            byte[] payload = encrypt(clear, key, nonce, ad);
            result.add(payload);
        } catch (Exception e) {
            LOGGER.warning("Unable to encrypt message: " + e.toString());
            return null;
        }
        // final Cbor packing.
        CBORObject cbor = CBORObject.FromObject(result);
        return CBORUtils.cborToBytes(cbor);
    }

    public Message buildMSG(Device dev, List<UUID> targets, MessageType msgType,String action) {
        Message message = new Message();
        if (dev!=null) {
            message.setSource(dev.getAddress());
            message.setDevType(dev.getDevType());
        }
        message.setTargets(targets);
        message.setTimestamp(buildTimeStamp());
        message.setMsgType(msgType);
        message.setAction(action);
        return message;
    }

    public Message buildMSG(Device dev, List<UUID> targets, MessageType msgType,String action, CBORObject body) {
        Message message = buildMSG(dev, targets, msgType, action);
        if (body!=null)
            message.setBody(body);
        return message;
    }

    public Message buildError(Device dev, List<UUID> targets, int i, String string) {
        return null;
    }

    public Message buildAlive(Device dev, List<UUID> targets, int timeout) {
        Message message = buildMSG(dev, targets, MessageType.NOTIFY, "alive");
        HashMap<String,Object> body = new HashMap<String,Object>();
        body.put("timeout", timeout);
        message.setBodyAsMap(body);
        return message;
    }

    public static byte[] decrypt(byte[] cipherText, byte[] key, byte[] nonce, byte[] ad) throws Exception {
        Cipher cipher = Cipher.getInstance("ChaCha20-Poly1305/None/NoPadding");
        AlgorithmParameterSpec ivParameterSpec = new IvParameterSpec(nonce);
        SecretKeySpec keySpec = new SecretKeySpec(key, "ChaCha20");
        cipher.init(Cipher.DECRYPT_MODE, keySpec, ivParameterSpec);
        cipher.updateAAD(ad);
        return cipher.doFinal(cipherText);
    }

    public static byte[] encrypt(byte[] clearText, byte[] key, byte[] nonce, byte[] ad) throws Exception {
        Cipher cipher = Cipher.getInstance("ChaCha20-Poly1305/None/NoPadding");
        AlgorithmParameterSpec ivParameterSpec = new IvParameterSpec(nonce);
        SecretKeySpec keySpec = new SecretKeySpec(key, "ChaCha20");
        cipher.init(Cipher.ENCRYPT_MODE, keySpec, ivParameterSpec);
        cipher.updateAAD(ad);
        return cipher.doFinal(clearText);
    }

    public static byte[] buildNonce(long[] timestamp) {
        byte[] nonce = new byte[12];
        System.arraycopy(BinUtils.longToByteArray(timestamp[0],8), 0, nonce, 0, 8);
        System.arraycopy(BinUtils.longToByteArray(timestamp[1],4), 0, nonce, 8, 4);
        return nonce;
    }

    public static long[] buildTimeStamp() {
        // Java don't provide a microsecond accurate clock
        // so we compute on millisecond and add a random seed
        long now = new Date().getTime();
        long ts1 = now / 1000;
        long ts2 = now % 1000 * 1000 + random.nextInt(999);
        long[] r = {ts1, ts2};
        return r;
    }
}
