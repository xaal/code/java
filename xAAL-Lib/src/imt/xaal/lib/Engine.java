package imt.xaal.lib;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.logging.Logger;

import imt.xaal.utils.BinUtils;

/**
 *
 * @author jkx
 */
public class Engine extends Thread {

    private final static Logger LOGGER = Logger.getLogger(Engine.class.getName());

    private CopyOnWriteArrayList<Device> devices = new CopyOnWriteArrayList<Device>();
    private NetworkConnector networkConnector = null;
    private MessageFactory factory = null;
    private ConcurrentLinkedQueue<byte[]> fifoIN = new ConcurrentLinkedQueue<byte[]>();
    private ConcurrentLinkedQueue<byte[]> fifoOUT = new ConcurrentLinkedQueue<byte[]>();
    private ConcurrentLinkedQueue<Attribute> attrChgQueue = new ConcurrentLinkedQueue<Attribute>();
    private boolean pause = false;

    public Engine() throws IOException {
        Config cfg = Config.getInstance();
        factory = new MessageFactory(BinUtils.unhexlify(cfg.getKey()));
        networkConnector = new NetworkConnector(cfg.getAddress(), cfg.getPort(), cfg.getHops());
        networkConnector.setOUTFifo(fifoIN);
    }

    public NetworkConnector getNetworkConnector() {
        return networkConnector;
    }

    public MessageFactory getMessageFactory() {
        return factory;
    }

    //##########################################################################
    // Device Management
    //##########################################################################
    /**
     * Ask the Engine to monitor some devices.
     *
     * @param devices List of device to monitor
     */
    public void registerDevices(List<Device> devices) {
        this.devices.addAll(devices);
        for (Device dev : devices) {
            dev.setEngine(this);
        }
    }

    /**
     * Ask the Engine to stop monitoring devices.
     *
     * @param devices the list of devices
     */
    public void unregisterDevices(List<Device> devices) {
        this.devices.removeAll(devices);
        for (Device dev : devices) {
            dev.setEngine(null);
        }
    }
   
    /**
     * Ask the Engine to monitor a device.
     *
     * @param device 
     */
    public void registerDevice(Device device) {
        this.devices.add(device);
        device.setEngine(this);
    }

    /**
     * Ask the Engine to stop monitoring device.
     *
     * @param device
     */
    public void unregisterDevice(Device device) {
        this.devices.remove(device);
        device.setEngine(null);
    }
    
    
    /**
     * Get the monitored devices.
     *
     * @return List of monitored devices
     */
    public List<Device> getDevices() {
        return devices;
    }

    public List<Device> filterDevicesForMessage(Message msg) {
        // return the device list for a given message 
        ArrayList<Device> result = new ArrayList<Device>();

        // is this a broadcast ? 
        if (msg.getTargets().isEmpty()) {
            result.addAll(getDevices());
            return result;
        }

        // is this a alive request ? 
        if (msg.isRequestIsAlive()) {
            LOGGER.severe("TBD filterDevicesForMessage: Filter isAlive");
            result.addAll(getDevices());
        } else {
            // are we in targets ? 
            for (Device dev : getDevices()) {
                if (msg.getTargets().contains(dev.getAddress())) {
                    result.add(dev);
                }
            }

        }
        return result;
    }

    //##########################################################################
    // AttributesChange
    //##########################################################################
    /**
     * Add a new attribute change, to the queue.
     *
     * @param attr the freshly changed attribute
     */
    public void registerAttributeChange(Attribute attr) {
        attrChgQueue.add(attr);
    }

    /**
     * Queue the attributesChange message for each device.
     * @throws Exception
     */
    public void processAttributesChange() {
        HashMap<Device, HashMap<String, Object>> map = new HashMap<Device, HashMap<String, Object>>();

        // group attributeChange by device, and store it in a HashMap
        for (Attribute attr : attrChgQueue) {
            Device dev = attr.getDevice();
            if (!map.containsKey(dev)) {
                map.put(dev, new HashMap<String, Object>());
            }
            map.get(dev).put(attr.getName(), attr.getValue());
        }
        attrChgQueue.clear();

        // for each device produce a new msg
        for (Device key : map.keySet()) {
            Message msg = factory.buildMSG(key, Arrays.asList(), MessageType.NOTIFY, "attributes_change");
            LOGGER.info("AttributesChange : " + key + "=>" + map.get(key));
            msg.setBodyAsMap(map.get(key));
            queueMessage(msg);
        }
    }

    /**
     * Queue Alive message for devices
     */
    public void processAlives() {
        long now = System.currentTimeMillis()/1000;
        
        for (Device dev : getDevices()) {
            if (dev.getAliveTimeOut() < now) {
                Message msg = factory.buildAlive(dev, new ArrayList<UUID>(), dev.getTimeOutPeriod());
                LOGGER.info("Sending Alive for " + dev);
                queueMessage(msg);
                dev.updateAliveTimeOut();
            }
        }
    }

 
    /**
     * Answer to the getDescription request
     *
     * @param dev the device (that should answer)
     * @param msg the incoming request
     */
    public void handleGetDescription(Device dev, Message msg) {
        HashMap<String,Object> body = new HashMap<String,Object>();
        if (dev.getVendorID()!=null)  body.put("vendor_id", dev.getVendorID());
        if (dev.getProductID()!=null) body.put("product_id", dev.getProductID());
        if (dev.getVersion()!=null)   body.put("version", dev.getVersion());
        if (dev.getUrl()!=null)       body.put("url", dev.getUrl());
        if (dev.getInfo()!=null)      body.put("info", dev.getInfo());
        if (dev.getHwID()!=null)      body.put("hw_id", dev.getHwID());
        if (dev.getSchema()!=null)    body.put("schema", dev.getSchema());
        if (dev.getGroupID() != null) body.put("group_id", dev.getGroupID());

        if (dev.getUnsupportedMethods().isEmpty() == false)
           body.put("unsupported_methods", dev.getUnsupportedMethods());
        if (dev.getUnsupportedAttributes().isEmpty() == false)
            body.put("unsupported_attributes", dev.getUnsupportedAttributes());
        if (dev.getUnsupportedNotifications().isEmpty() == false)
            body.put("unsupported_notifications", dev.getUnsupportedNotifications());

        Message reply = factory.buildMSG(dev, Arrays.asList(msg.getSource()), MessageType.REPLY, msg.getAction());
        reply.setBodyAsMap(body);
        queueMessage(reply);
    }

    /**
     * Answer to getAttributes request
     *
     * @param dev the device (that should answer)
     * @param msg the incoming request
     */
    public void handleGetAttributes(Device dev, Message msg) {
        HashMap<String,Object> body = new HashMap<String,Object>();
        for (Attribute attr : dev.getAttributes()) {
            body.put(attr.getName(), attr.getValue());
        }
        Message reply = factory.buildMSG(dev, Arrays.asList(msg.getSource()), MessageType.REPLY, msg.getAction());
        reply.setBodyAsMap(body);
        queueMessage(reply);
    }

    /**
     * Answer to a isAlive request
     *
     * @param dev the device (that should answer)
     * @param msg the incoming request
     */
    public void handleIsAlive(Device dev, Message msg) {
        Message reply = factory.buildAlive(dev, Arrays.asList(msg.getSource()), dev.getTimeOutPeriod());
        queueMessage(reply);
    }

    /**
     * Handle the missing methods. aka ToBeDone
     *
     * @param dev the device (that should answer)
     * @param msg the incoming request
     */
    public void handleTDB(Device dev, Message msg) {
        Message error = factory.buildError(dev, Arrays.asList(msg.getSource()), 500, "Not implemented yet");
        queueMessage(error);
    }

    /**
     * Request dispatching.
     *
     * @param targets list of device that should handle an incoming message
     * @param msg the message to handle.
     */
    public void handleRequest(List<Device> targets, Message msg) {

        for (Device dev : targets) {
            LOGGER.info("REQUEST " + msg.getAction() + " for : " + dev.toString());

            switch (msg.getAction()) {
                case "is_alive":
                    handleIsAlive(dev, msg);
                    break;
                case "get_description":
                    handleGetDescription(dev, msg);
                    break;
                case "get_attributes":
                    handleGetAttributes(dev, msg);
                    break;

                default:
                    Message result = dev.handleRequest(msg);
                    if (result != null) {
                        queueMessage(result);
                    }
                    break;
            }

        }

    }

    //##########################################################################
    // process[Rx|Tx] & queueMSG
    //##########################################################################
    
    /**
     * Push a message in the output queue.
     *
     * @param msg the message to queue.
     */
    public void queueMessage(Message msg) {
        try {
            byte[] buf = factory.encodeMessage(msg);
            if (buf != null)
                fifoOUT.add(buf);
        } catch (IOException e) {
            LOGGER.warning("Unable to encode msg: " + e.toString());
        }
    }

    /**
     * Send a single data buffer
     *
     * @param data the buffer to send
     */
    public void processTx(byte[] data) {
        //LOGGER.info(data);
        networkConnector.send(data);
    }

    /**
     * Process an incoming message, by default, we only answer to REQUEST.
     * Override this if you want to re-route RxMSG process to dump incoming
     * message for example
     *
     * @param msg the message to process
     */
    public void processRxMSG(Message msg) {
        if (msg.getMsgType() == MessageType.REQUEST) {
            List<Device> targets = filterDevicesForMessage(msg);
            handleRequest(targets, msg);
        }
    }

    //##########################################################################
    // Loop start / stop 
    //##########################################################################
    
    /**
     * Start the network Thread.
     * @throws IOException
     */
    public void init() throws IOException {
        networkConnector.connect();
        networkConnector.start();
    }

    /**
     * Pause the network Thread.
     */
    public void pause() {
        LOGGER.info("Pausing network connector");
        networkConnector.pause();
        pause = true;
    }

    /**
     * Engine loop. Caution : we must can this frequently to push & pull the
     * network fifos. We don't loop trough the fifos, and only flush the first
     * inserted message.
     * @throws Exception
     */
    public void loop() throws Exception {
        // handle a new incomming packet
        if (!fifoIN.isEmpty()) {
            byte[] data = fifoIN.poll();
            Message msg = factory.decodeMessage(data);
            if (msg!=null)
                processRxMSG(msg);
        }

        // AttributesChange ? 
        if (!attrChgQueue.isEmpty()) {
            processAttributesChange();
        }
        
        // Alive timeout ? 
        processAlives();

        // is there something to send 
        if (!fifoOUT.isEmpty()) {
            byte[] data = fifoOUT.poll();
            processTx(data);
        }
    }

    /**
     * Main Engine loop.
     */
    public void run() {
        while (pause == false) {
            sleep(); // see below
            try {
                loop();
            } catch (Exception ex) {
                LOGGER.severe(ex.getMessage());
            }
        }
    }

    /**
     * Sleep to avoid CPU hog.
     */
    private void sleep() {
        try {
            Thread.sleep(20);
        } catch (InterruptedException ex) {
            LOGGER.info(ex.toString());
            LOGGER.info(ex.getStackTrace().toString());
        }
    }
}
