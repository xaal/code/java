package imt.xaal.utils;

import java.net.DatagramSocket;
import java.net.InetAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.net.UnknownHostException;
import java.util.logging.Logger;

import imt.xaal.lib.NetworkConnector;

public final class NetworkUtils {

    private final static Logger LOGGER = Logger.getLogger(NetworkConnector.class.getName());

    public static String getDefaultNetworkInterface() {
        // https://stackoverflow.com/questions/42102601/getting-the-default-network-interface-via-java

        final String globalHost = "a.root-servers.net"; // Must exist.
        String name = null;
        InetAddress remoteAddress = null;

        try {
            remoteAddress = InetAddress.getByName(globalHost);
        } catch (UnknownHostException e) {
            LOGGER.warning("Unable to resolve " + globalHost);
        }

        if (remoteAddress != null) {
            try (DatagramSocket s = new DatagramSocket()) {
                // UDP does not actually open a connection, we just need to do this to get the
                // interface we would send packets on if we actually tried.
                s.connect(remoteAddress, 80);
                name = NetworkInterface.getByInetAddress(s.getLocalAddress()).getName();
            } catch (SocketException e) {
                LOGGER.warning("Unable to get default network interface");
            }
        }
        return name;
    }
}
