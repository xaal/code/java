package imt.xaal.utils;

import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;

import com.upokecenter.cbor.CBORObject;

public class CBORUtils {

    public static CBORObject bytesToCbor(byte [] data){
        return CBORObject.Read(new ByteArrayInputStream(data));
    }

    public static byte[] cborToBytes(CBORObject cbor) throws IOException {
        var stream = new ByteArrayOutputStream();
        cbor.WriteTo(stream);
        return stream.toByteArray();
    }

}
