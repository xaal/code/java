# xAAL Lib for Java

This repo contains the Java implementation of the xAAL protocol.
It provides a set of classes to create xAAL devices and gateways,
and a sample usage of these classes.

## Usage
```java
package imt.xaal.tests;

import java.util.Arrays;
import java.util.UUID;

import imt.xaal.lib.Engine;

public class TestLamp {

    public static void main(String[] args) throws Exception{

        LogFormatter.install();
        Engine eng = new Engine();
        Lamp l1 = new Lamp(UUID.fromString("6265eb30-8c59-11e9-98b1-b827ebe99201"));
        Lamp l2 = new Lamp(UUID.fromString("6265eb30-8c59-11e9-98b1-b827ebe99202"));
        UUID grp = UUID.randomUUID();
        l1.setGroupID(grp);
        l2.setGroupID(grp);

        eng.registerDevices(Arrays.asList(l1,l2));
        eng.init();
        eng.start();
        
        l1.toggle();
        l2.setBrightness(50);
    }    
    
}
```


License: LGPL v3
