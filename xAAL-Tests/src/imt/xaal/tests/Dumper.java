package imt.xaal.tests;

import imt.xaal.lib.Engine;
import imt.xaal.lib.Message;

import java.io.IOException;


public class Dumper extends Engine {

    public Dumper() throws IOException {
        super();
    }

    @Override
    public void processRxMSG(Message msg) {
        msg.dump();
    }

    public static void main (String[] args) throws IOException {
        LogFormatter.install();
        Dumper dumper = new Dumper();
        dumper.init();
        dumper.start();
    }
}
