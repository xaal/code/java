package imt.xaal.tests;

import java.net.URI;
import java.net.URISyntaxException;
import java.util.Arrays;
import java.util.UUID;

import imt.xaal.lib.Attribute;
import imt.xaal.lib.Device;
import imt.xaal.lib.Message;


public class Lamp extends Device {
    Attribute light = new Attribute("light", false);
    Attribute brightness = new Attribute("brightness", 100);


    public Lamp(UUID address) throws URISyntaxException{
        super(address);
        setDevType("lamp.dimmer");
        setVendorID("Jkx Industries");
        setVersion("35 rev2");
        setProductID("Dummy Java Lamp");
        setHwID("6b2");
        setInfo(getProductID()+ " "  + getHwID());
        setSchema( new URI("http://foobar.com"));
        setUrl(new URI("http://dev.foobar.org"));
        registerAttributes(Arrays.asList(light,brightness));
    }

    public void toggle() {
        var state = (boolean)light.getValue();
        if (state==true)
            light.setValue(false);
        else
            light.setValue(true);
    }

    public void turn_on() {
        light.setValue(true);
    }

    public void turn_off() {
        light.setValue(false);
    }

    public void setBrightness(Integer target) {
        if (target<=0) {
            turn_off();
            target = 0;
        }
        else {
            if ((boolean)light.getValue()==false) 
                turn_on();
        }   
        brightness.setValue(target);        
    }

    @Override
    public Message handleRequest(Message msg) {
        switch (msg.getAction()) {
            case "toggle":
                toggle();
                break;
            case "turn_on":
                turn_on();
                break;
            case "turn_off":
                turn_off();
                break;
            case "set_brightness":
                Integer target = msg.getBodyAsMap().get("brightness").AsInt32();
                setBrightness(target);
                break;
        }
        return null;
    }
    
}
